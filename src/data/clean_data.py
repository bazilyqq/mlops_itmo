import click
import numpy as np
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path())
@click.argument('output_path', type=click.Path())
def clean_data(input_path: str, output_path: str):
    """Function remove passed values from the dataset
    :param input_path: Path to read DataFrame
    :param output_path: Path to save cleaned DataFrame
    :return:
    """
    df = pd.read_csv(input_path)

    df.replace(" ", np.nan, inplace=True)
    df["TotalSpent"] = df.TotalSpent.fillna(0).astype(float)

    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    clean_data()
