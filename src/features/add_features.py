import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path())
@click.argument('output_path', type=click.Path())
def add_features(input_path: str, output_path: str):
    """Adding features to dataset
    :param input_path: Path to read cleaned DataFrame
    :param output_path: Path to save featured DataFrame
    :return:
    """
    df = pd.read_csv(input_path)

    patterns = {
        "No": 0,
        "No internet service": 0,
        "No phone service": 0,
        "Yes": 1,
        "Male": 0,
        "Female": 1,
        "DSL": 1,
        "Fiber optic": 2,
        "Month-to-month": 0,
        "One year": 1,
        "Two year": 2,
        "Credit card (automatic)": 0,
        "Bank transfer (automatic)": 1,
        "Mailed check": 2,
        "Electronic check": 3
    }
    df = df.replace(patterns)

    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    add_features()
