import os
import pickle

import click
import mlflow
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

mlflow.set_tracking_uri('http://80.90.181.131:5000')
mlflow.set_experiment('test5')
os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://80.90.181.131:9000'

@click.command()
@click.argument('input_path', type=click.Path())
@click.argument('output_path', type=click.Path())
def train_logreg(input_path: str, output_path: str):
    """Function activates train loop of logistic regression model
    :param input_path: Path to read featured DataFrame
    :param output_path: Path to save trained model
    :return:
    """
    with mlflow.start_run():
        df = pd.read_csv(input_path)

        x_train = df.drop('Churn', axis=1)
        y_train = df['Churn']

        params = {
            "model__C": [1.734],  # np.arange(0.001, 100, 0.001)
        }
        clf = Pipeline(steps=[
            ("scaler", StandardScaler()),
            ("model", LogisticRegression(penalty="l1", solver="saga", max_iter=1000, random_state=42)),
        ])
        grid_search = GridSearchCV(
            estimator=clf,
            param_grid=params,
            scoring="roc_auc",
            n_jobs=-1,
            cv=10,
            refit=True,
        )
        logreg = grid_search.fit(x_train, y_train)

        pickle.dump(logreg, open(output_path, "wb"))

    mlflow.log_params(logreg.best_params_)
    mlflow.log_metrics(dict(roc_auc=logreg.best_score_))

if __name__ == "__main__":
    train_logreg()
