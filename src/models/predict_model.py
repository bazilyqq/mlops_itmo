import pickle

import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path())
@click.argument('model_path', type=click.Path())
@click.argument('output_path', type=click.Path())
def predict_model(input_path: str, model_path: str, output_path: str):
    """Function predicts values using trained model
    :param input_path: Path to read data for predict
    :param model_path: Path contains trained model
    :param output_path: Path to save predicted values
    :return:
    """
    df = pd.read_csv(input_path)
    model = pickle.load(open(model_path, "rb"))
    submission = model.predict_proba(df)[:, 1]
    submission = pd.DataFrame(submission)
    submission.to_csv(output_path)


if __name__ == "__main__":
    predict_model()
